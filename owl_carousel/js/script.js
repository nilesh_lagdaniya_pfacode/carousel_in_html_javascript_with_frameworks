$(document).ready(function (){
    $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        margin:10,
        responsiveClass:true,
        responsive:true,
        center:true,
        lazyLoad:true,
        animateOut:'fadeOut',
        navText:[
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        nav:true,
        responsive:{
            
            0:{
                items:1,
            },
            400:{
                items:2,
                margin:0,
                stagePadding: 20,
            },
            600:{
                items:2,
                stagePadding: 10,

            },
            800:{
                items:2,
                stagePadding: 10,
            },
            1000:{
                items:3,
                stagePadding: 15

            },
            1200:{
                items:4,
                stagePadding: 10
            },
        }
    })
})
