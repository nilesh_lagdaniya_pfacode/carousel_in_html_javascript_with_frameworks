$('.slider').slick({
    dots:true,
    infinite:true,
    centerMode:true,
    centerPadding:'30px',
    autoplay:true,
    autoplaySpeed:1000,
    slideToShow:3,
    fade:true,
    cssEase:'linear'
    
})